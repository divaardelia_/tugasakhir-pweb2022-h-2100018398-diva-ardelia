<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="bio.css">
</head>
<body>
	<script type="text/javascript">
		function klik() {
			var Nama = document.getElementById("nama").value;
			var Nim = document.getElementById("nim").value;
			var Kelas = document.getElementById("kelas").value;
			var Gender = document.getElementById("gender").value;
			var Email = document.getElementById("email").value;
			if (Nama!="" && Nim!="" && Kelas!="" && Gender!="" && Email!="") {
				return true;
			}
			else{
				alert('Data yang dimasukkan tidak lengkap!')
			}
		}
	</script>
	<form name="fform" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<div class="input">
			<h1>Input Data</h1><br>
			<p>
				<input class="ipt" type="text" name="nama" placeholder="Nama.." id="nama">
			</p>
			<p>
				<input class="ipt" type="text" name="nim" placeholder="NIM" id="nim">
			</p>
			<p>
				<input class="ipt" type="text" name="kelas" placeholder="Kelas" id="kelas">
			</p>
			<p class="gdr"> *Gender <br><br>
				<input  type="checkbox" name="gender1" value="P" id="gender"> Perempuan
				<input  type="checkbox" name="gender2" value="L" id="gender"> Laki-Laki 
			</p>
			<p>
				<input class="ipt" type="email" name="email" placeholder="Email" id="email">
			</p>
			<p>
				<input class="ipt" type="date" name="ttl" placeholder="TanggalLahir" id="ttl">
			</p>
			<p>
				<button type="submit" onclick="klik()" value="kirim">Kirim</button>
				<button type="reset">Reset</button>
			</p>
		</div>
	</form>
	<div class="output">
	<?php

	if($_SERVER["REQUEST_METHOD"] ==  "POST"){
		$namastr = $_POST['nama'];
		$nimstr = $_POST['nim'];
		$kelastr = $_POST['kelas'];
		$emailstr = $_POST['email'];
		if(isset($_POST['gender1'])){
			$gender = $_POST['gender1'];
		}
		if(isset($_POST['gender2'])){
			$gender = $_POST['gender2'];
		}
		$tanggalstr = $_POST['ttl'];

		if ($namastr!="" && $nimstr!="" && $kelastr!="" && $emailstr!="" && $tanggalstr!="") {
			$fp = fopen("tugasakhir.txt", "a+");
			fputs($fp, "$nimstr|$namastr|$kelastr|$tanggalstr|$emailstr|$gender\n");
			fclose($fp);

			echo "<left>";
			echo "<h1>Your Input</h1>";
			echo "<p>Nama : $namastr</p>";
			echo "<p>Nim : $nimstr</p>";
			echo "<p>Kelas : $kelastr</p>";
			if(isset($_POST['gender1'])){
				echo "<p>Gender : ".$_POST['gender1'];
			}
			if(isset($_POST['gender2'])){
				echo "<p>Gender : ".$_POST['gender2'];
			}
			echo "<p>Email : $emailstr</p>";
			echo "<p>Tanggal Lahir : $tanggalstr</p>";
			echo "</left>";
		}
	}
	echo "<br><br>";
	echo "<a href=tugasakhir.html>::Kembali ke Halaman Utama</a>";
	?>
	</div>
</body>
</html>